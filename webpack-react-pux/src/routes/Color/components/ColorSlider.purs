module ColorSlider
  ( getReactClass
  , reducer
  , initialState
  ) where

import Prelude (bind, discard, map, pure, show, unit, ($), ($>), (<<<))

import Control.Monad.Aff (Aff)
import Control.Monad.Eff (Eff, kind Effect)
import Control.Monad.Eff.Class (liftEff)
import Data.Maybe (Maybe(..), fromJust)
import Partial.Unsafe (unsafePartial)
import Pux (CoreEffects, FoldP, start, noEffects)
import Pux.DOM.HTML (HTML)
import Pux.DOM.HTML.Attributes (style)
import Pux.Redux (Action, AppEvent(..), Dispatch, REDUX, Reducer, appEvent, fromAppConfig)
import Pux.Renderer.React (renderToReact)
import React (ReactClass)
import Signal (Signal)
import Text.Smolder.Markup (text, (!), (#!))

import CSS as CSS
import CSS.Common (auto) as CSS
import CSS.VerticalAlign (textBottom, verticalAlign) as CSS
import Data.Int as Int
import Pux.DOM.Events as E
import Text.Smolder.HTML as HTML
import Text.Smolder.HTML.Attributes as A

data Event
  = SetRed Int
  | SetGreen Int
  | SetBlue Int
  | SetState State

type State =
  { red :: Int
  , green :: Int
  , blue :: Int
  }

type Payload = (payload :: State)

reducer :: Reducer State Payload
reducer st action =
  case action.type of
    "UPDATE_COLOR" -> action.payload
    _ -> st

foldp
  :: forall fx
   . Dispatch Payload fx
  -> FoldP State Event (redux :: REDUX | fx)
foldp dispatch ev st =
  case ev of
    SetRed r ->
      let st' = st { red = r }
      in { state: st', effects: [ sendEvent st' ] }
    SetGreen g ->
      let st' = st { green = g }
      in { state: st', effects: [ sendEvent st' ] }
    SetBlue b ->
      let st' = st { blue = b }
      in { state: st', effects: [ sendEvent st' ] }
    SetState state -> noEffects state

  where
    toAction :: State -> Action Payload
    toAction input = { type: "UPDATE_COLOR", payload: input }

    sendEvent :: State -> Aff (CoreEffects (redux :: REDUX | fx)) (Maybe Event)
    sendEvent input = liftEff (dispatch $ toAction input) $> Nothing

view :: forall pl fx. State -> HTML (AppEvent pl fx Event)
view st = unsafePartial $ HTML.div do
  HTML.div ! style do
      CSS.backgroundColor (CSS.rgb st.red st.green st.blue)
      CSS.height (CSS.px 100.0)
      CSS.width (CSS.px 100.0)
      CSS.marginLeft CSS.auto
      CSS.marginRight CSS.auto
    $ pure unit
  HTML.br

  HTML.label do
    text "Red: "
    HTML.input
      ! A.id "red-slider"
      ! A.type' "range"
      ! A.min "0"
      ! A.max "255"
      ! A.value (show st.red)
      #! appEvent E.onChange (SetRed <<< targetAsInt)
      ! style do
        CSS.verticalAlign CSS.textBottom
  HTML.br

  HTML.label do
    text "Green: "
    HTML.input
      ! A.id "green-slider"
      ! A.type' "range"
      ! A.min "0"
      ! A.max "255"
      ! A.value (show st.green)
      #! appEvent E.onChange (SetGreen <<< targetAsInt)
      ! style do
        CSS.verticalAlign CSS.textBottom
  HTML.br

  HTML.label do
    text "Blue: "
    HTML.input
      ! A.id "blue-slider"
      ! A.type' "range"
      ! A.min "0"
      ! A.max "255"
      ! A.value (show st.blue)
      #! appEvent E.onChange (SetBlue <<< targetAsInt)
      ! style do
        CSS.verticalAlign CSS.textBottom
  HTML.br

  where
    targetAsInt :: Partial => E.DOMEvent -> Int
    targetAsInt = fromJust <<< Int.fromString <<< E.targetValue

initialState :: State
initialState = {red: 0, green: 0, blue: 0}

getReactClass
  :: forall fx props
   . Signal (Dispatch Payload fx)
  -> Signal State
  -> Eff (CoreEffects (redux :: REDUX | fx)) (ReactClass props) 
getReactClass dispatch stateSignal = do
  app <- start $ fromAppConfig
    { initialState
    , view
    , foldp
    , dispatch
    , inputs: [ map SetState stateSignal ]
    }

  renderToReact app.markup app.input
