import * as React from 'react'
import { injectReducer } from '../../store/reducers'

export default (store) => ({
  path : 'color',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const Color = require('./containers/ColorSlider').default
      const reducer = require('./components/ColorSlider').reducer
      const initialState = require('./components/ColorSlider').initialState

      injectReducer(
          store,
          {
              key: 'color',
              reducer: (state = initialState, action) => reducer(state)(action)
          }
      )

      /*  Return getComponent   */
      cb(null, Color)

    /* Webpack named bundle   */
    }, 'color')
  }
})
