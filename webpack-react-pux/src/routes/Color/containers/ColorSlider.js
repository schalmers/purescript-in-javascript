import { connect } from 'react-redux'
import * as React from 'react'
import * as ColorSliderModule from '../components/ColorSlider'
import { constant } from 'purescript-signal/src/Signal.purs'
import { initialDispatch } from 'purescript-pux-redux/src/Pux/Redux.purs'

const mapStateToProps = (state) => ({
    red: state.color.red,
    green: state.color.green,
    blue: state.color.blue,
})

const stateSignal = constant(ColorSliderModule.initialState)
const dispatchSignal = constant(initialDispatch)

const ColorSlider =
    ColorSliderModule.getReactClass(dispatchSignal)(stateSignal)()

class ColorContainer extends React.Component {
    constructor(props) {
        super(props)
        dispatchSignal.set((action) => () => props.dispatch(action))
    }

    sendState(props, signal) {
        signal.set({
            red: props.red || 0,
            green: props.green || 0,
            blue: props.blue || 0
        })
    }

    componentWillMount() {
        this.sendState(this.props, stateSignal)
    }

    componentWillReceiveProps(nextProps) {
        this.sendState(nextProps, stateSignal)
    }

    render() {
        return <ColorSlider />
    }
}

export default connect(mapStateToProps, null)(ColorContainer)
