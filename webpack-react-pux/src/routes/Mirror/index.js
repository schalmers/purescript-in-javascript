import * as React from 'react'

export default (store) => ({
  path : 'mirror',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const Mirror = (store) =>
            React.createElement(require('./Mirror').getReactClass())

      /*  Return getComponent   */
      cb(null, Mirror)

    /* Webpack named bundle   */
    }, 'mirror')
  }
})
