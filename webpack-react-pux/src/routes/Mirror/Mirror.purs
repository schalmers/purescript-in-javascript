module Mirror (getReactClass) where

import Prelude

import Control.Monad.Eff (Eff)
import Pux (CoreEffects, FoldP, start)
import Pux.DOM.Events (onClick, onInput, targetValue)
import Pux.DOM.HTML (HTML)
import Pux.Renderer.React (renderToReact)
import React (ReactClass)
import Text.Smolder.HTML as HTML
import Text.Smolder.HTML.Attributes as A
import Text.Smolder.Markup (text, with, (!), (#!))

data Event
  = UpdateText String
  | ClearText

type State = { text :: String }

foldp :: forall fx. FoldP State Event fx
foldp (UpdateText str) st =
  { state: st { text = str }
  , effects: []
  }
foldp ClearText st =
  { state: st { text = "" }
  , effects: []
  }

view :: State -> HTML Event
view st = HTML.div do
  with HTML.h2 (A.style "height: 1em;") <<< text $ st.text
  with HTML.div (A.className "input-group") do
    HTML.input
      ! A.type' "text"
      ! A.className "form-control"
      ! A.value st.text
      #! onInput (UpdateText <<< targetValue)
    HTML.button
      ! A.className "btn btn-secondary input-group-addon"
      #! onClick (const ClearText) $ text "clear"

getReactClass :: forall fx props. Eff (CoreEffects fx) (ReactClass props) 
getReactClass = do
  app <- start
    { initialState: { text: "" }
    , view
    , foldp
    , inputs: []
    }

  renderToReact app.markup app.input
