Purescript in Javascript
========================

A collection of documentation, example projects, and code snippets to help people
through the process of integrating Purescript code into existing Javascript codebases.

## Contents

1. `gulp-minimal` - simple example of calling a Purescript function from Javascript and how to
integrate Purescript into a `gulp` build

2. `gulp-effects` - calling an effectful Purescript program from Javascript

3. `webpack-react-pux` - Extending `react` applications using Purescript, and building Purescript with `webpack`

    a. Writing a `react` component using `purescript-pux` and using it in Javascript

    b. Writing a `purescript-pux` component that can control the app's `redux` store.
