module Search where

import Prelude

import Control.Monad.Aff (Aff, launchAff)
import Control.Monad.Aff.Console (CONSOLE, log)
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Exception (EXCEPTION)
import Data.Argonaut (class DecodeJson, decodeJson, (.?), toObject)
import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Network.HTTP.Affjax (AJAX, get)

newtype SearchResponse
  = SearchResponse
  { term :: String
  , count :: Int
  }

displaySearchResponse :: SearchResponse -> String
displaySearchResponse (SearchResponse resp) =
  "Your term was: " <> resp.term <> ". Your count was: " <> show resp.count <> "."

instance decodeJsonSearchResponse :: DecodeJson SearchResponse where
  decodeJson json =
    case toObject json of
      Just object -> do
        term <- object .? "term"
        count <- object .? "count"
        pure $ SearchResponse { term, count }
      Nothing -> Left "Not a JSON object"

runSearch :: forall e. String -> Int -> Aff (ajax :: AJAX | e) (Either String SearchResponse)
runSearch term count = do
  resp <- get $
    "http://localhost:8000/search?term=" <>
    term <>
    "&count=" <>
    show count
  pure <<< decodeJson $ resp.response

search :: forall e. String -> Int -> Eff (exception :: EXCEPTION, ajax :: AJAX, console :: CONSOLE | e) Unit
search term count = void $ launchAff $ do
  resp <- runSearch term count
  log $ case resp of
    Left err -> "Error: " <> err
    Right searchResp -> displaySearchResponse searchResp
