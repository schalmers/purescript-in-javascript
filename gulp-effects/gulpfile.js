var execSync = require("child_process").execSync;
var fs = require("fs");
var gulp = require("gulp");
var path = require("path");
var prepack = require("prepack");

var src_dir = "./src";
var dist_dir = "./dist";
var output_dir = "./output";

var output_file = "output.js";

gulp.task("bower", function() {
    return require("bower").commands.install();
});

gulp.task("purs", ["bower"], function() {
    var purs_sources = [
        path.join(src_dir, "purescript/**/*.purs"),
        "./bower_components/purescript-*/src/**/*.purs"
    ];
    var purs_output = path.join(dist_dir, "purescript")

    execSync("mkdir -p " + purs_output);

    return require("gulp-purescript").compile({
        src: purs_sources,
        output: purs_output
    })
});

gulp.task("js", ["purs"], function() {
    var js_src = path.join(src_dir, "**/*.js");

    return gulp.src(js_src).pipe(gulp.dest(dist_dir));
});

gulp.task("browserify", ["js"], function() {
    execSync("mkdir -p " + output_dir);

    var options = {
        entries: "main.js",
        basedir: dist_dir,
    }

    return require("browserify")(options).bundle()
        .pipe(fs.createWriteStream(path.join(output_dir, output_file)));
});

gulp.task("build", ["browserify"], function() {
    return gulp.src("output/*.js").pipe(gulp.dest("server"));
});

gulp.task("run", ["build"], function(finish) {
    console.log("Server will be running on http://localhost:8000. View the page's console output.");
    execSync("node ./server/server.js");
    finish();
});
