var Hapi = require("hapi");

var server = new Hapi.Server({
    connections: {
        routes: {
            files: {
                relativeTo: __dirname
            }
        }
    }
});

server.connection({ port: 8000, host: "localhost" });

server.register(require("inert"), function(err) {
    if (err) {
        throw err;
    }

    server.route({
        method: "GET",
        path: "/{param*}",
        handler: {
            directory: {
                path: ".",
                index: true
            }
        }
    });

    server.route({
        method: "GET",
        path: "/search",
        handler: function (request, reply) {
            reply.response({
                term: request.query.term,
                count: parseInt(request.query.count)
            });
        }
    });

    server.start(function(err) {
        if (err) {
            throw err;
        }
        console.log("Server running at: " + server.info.uri);
    });
});

