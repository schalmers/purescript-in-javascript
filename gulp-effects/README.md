Purescript in Javascript with `gulp`
---

## Requirements

* purs - Purescript compiler
* bower - Purescript dependency management
* gulp - Project builds
  * gulp-purescript

## Recommendations

* browserify - Bundling CommonJS modules

To run this example, do `npm install` then `gulp run`

## Project Structure

```
purs-js-gulp-effects/
- output/                  (Browser-ready .js files)
- dist/                    (Compiled Purescript + JS sources)
- src/
  - purescript/            (Purescript source files)
    - Search.purs
  - main.js
- server/                  (Local server to test the app)
  - server.js
  - output.js
  - index.html
- bower.json               (Purescript dependencies)
- package.json
- gulpfile.js
```

This project is similar to the `minimal` example, but the Purescript function we're calling has type
`String -> Eff _ ()`. In Javascript land, a value of type `Eff _ a` is a function of zero arguments that 
does some things and returns an `a`.

Here's `main.js` this time:

`main.js`
```
var Search = require("./purescript/Search");

Search.search("This is a search term")(10)();
```

`search` is a function that sends an AJAX request to a local server. You can test it out by running
`gulp run` from this directory.

## `gulpfile.js` configuration

The build process is almost the same as in the `minimal` example, but the
`prepack` step has been excluded. Feel free to introduce your own dead
code elimination step instead.

0. Imports and configuration

    ```
    var browserify = require("browserify");
    var execSync = require("child_process").execSync;
    var fs = require("fs");
    var gulp = require("gulp");
    var path = require("path");
    var prepack = require("prepack");

    var src_dir = "./src";
    var dist_dir = "./dist";
    var output_dir = "./output";

    var output_file = "output.js";
    ```

1. Ensure Purescript dependencies are installed.

    ```
    gulp.task("bower", function() {
        return require("bower").commands.install();
    });
    ```

2. Compile Purescript modules

    ```
    gulp.task("purs", ["bower"], function() {
        var purs_sources = [
            path.join(src_dir, "purescript/**/*.purs"),
            "./bower_components/purescript-*/src/**/*.purs"
        ];
        var purs_output = path.join(dist_dir, "purescript")

        execSync("mkdir -p " + purs_output);

        return require("gulp-purescript").compile({
            src: purs_sources,
            output: purs_output
        })
    });
    ```

3. Compile Javascript files

    ```
    gulp.task("js", ["purs"], function() {
        var js_src = path.join(src_dir, "**/*.js");
        
        // You might want to do more here, like using babel
        return gulp.src(js_src).pipe(gulp.dest(dist_dir));
    });
    ```

4. Browserify compiled output

    ```
    gulp.task("browserify", ["js"], function() {
        execSync("mkdir -p " + output_dir);

        var options = {
            entries: "main.js",
            basedir: dist_dir,
        }

        return require("browserify")(options).bundle()
            .pipe(fs.createWriteStream(path.join(output_dir, output_file)));
    });
    ```

5. Copy output to server directory

    ```
    gulp.task("build", ["browserify"], function() {
        return gulp.src("output/*.js").pipe(gulp.dest("server"));
    });
    ```

6. Run server

    ```
    gulp.task("run", ["build"], function(finish) {
        execSync("node ./server/server.js");
        finish();
    });
    ```
