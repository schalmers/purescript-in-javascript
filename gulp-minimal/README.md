Purescript in Javascript with `gulp`
---

## Requirements

* purs - Purescript compiler
* bower - Purescript dependency management
* gulp - Project builds
  * gulp-purescript

## Recommendations

* browserify - Bundling CommonJS modules
* prepack - Dead code elimination

## Project Structure

```
purs-js-gulp-minimal/
- output/                  (Browser-ready .js files)
- dist/                    (Compiled Purescript + JS sources)
- src/
  - purescript/            (Purescript source files)
    - Factorial.purs
  - main.js
- bower.json               (Purescript dependencies)
- package.json
- gulpfile.js
```

Purescript modules are compiled to CommonJS modules, so they can be `require`d from regular Javascript.

Here's a Purescript module:

`purescript/Factorial.purs`
```
module Factorial where

import Prelude

factorial :: Int -> Int
factorial 0 = 1
factorial n = n * factorial (n - 1)
```

And a Javascript source file that uses it:

`main.js`
```
var Factorial = require('./purescript/Factorial.purs');

console.log(Factorial.factorial(8));
```

Simple. Some reminders:
* Nested Purescript modules, such as `Foo.Bar.Baz`, should be kept at the
corresponing path, i.e. `Foo/Bar/Baz.purs`
* All Purescript functions are curried by default, so remember to pass arguments one at a time

See the relevant
[Purescript by Example section](https://leanpub.com/purescript/read#leanpub-auto-calling-purescript-from-javascript) for more detail.

## `gulpfile.js` configuration

This example assumes you're using `gulp` to build your project.

0. Imports and configuration

    ```
    var browserify = require("browserify");
    var execSync = require("child_process").execSync;
    var fs = require("fs");
    var gulp = require("gulp");
    var path = require("path");
    var prepack = require("prepack");

    var src_dir = "./src";
    var dist_dir = "./dist";
    var output_dir = "./output";

    var output_file = "output.js";
    ```

1. Ensure Purescript dependencies are installed.

    ```
    gulp.task("bower", function() {
        return require("bower").commands.install();
    });
    ```

2. Compile Purescript modules

    ```
    gulp.task("purs", ["bower"], function() {
        var purs_sources = [
            path.join(src_dir, "purescript/**/*.purs"),
            "./bower_components/purescript-*/src/**/*.purs"
        ];
        var purs_output = path.join(dist_dir, "purescript")

        execSync("mkdir -p " + purs_output);

        return require("gulp-purescript").compile({
            src: purs_sources,
            output: purs_output
        })
    });
    ```

3. Compile Javascript files

    ```
    gulp.task("js", ["purs"], function() {
        var js_src = path.join(src_dir, "**/*.js");
        
        // You might want to do more here, like using babel
        return gulp.src(js_src).pipe(gulp.dest(dist_dir));
    });
    ```

4. Browserify compiled output

    ```
    gulp.task("browserify", ["js"], function() {
        execSync("mkdir -p " + output_dir);

        var options = {
            entries: "main.js",
            basedir: dist_dir,
        }

        return require("browserify")(options).bundle()
            .pipe(fs.createWriteStream(path.join(output_dir, output_file)));
    });
    ```

5. (Optional) Post-process bundled output

    ```
    gulp.task("prepack", ["browserify"], function(cb) {
        /* Note - I want to do this:
         *
         * var output = prepack.prepackFileSync(path.join(output_dir, output_file));
         * return fs.createWriteStream(path.join(output_dir, output_file + ".pack"))
         *     .write(output.code);
         *
         * But the prepack API seems to have a bug. Invoking the executable works, however
         */

        execSync(
            "./node_modules/prepack/bin/prepack.js " +
            path.join(output_dir, output_file) +
            " --out " +
            path.join(output_dir, output_file + ".pack")
        );
        cb()
    });
    ```

6. (Optional) A nice alias that does it all

    ```
    gulp.task("build", ["prepack"]);
    ```
