var execSync = require("child_process").execSync;
var fs = require("fs");
var gulp = require("gulp");
var path = require("path");
var prepack = require("prepack");

var src_dir = "./src";
var dist_dir = "./dist";
var output_dir = "./output";

var output_file = "output.js";

gulp.task("bower", function() {
    return require("bower").commands.install();
});

gulp.task("purs", ["bower"], function() {
    var purs_sources = [
        path.join(src_dir, "purescript/**/*.purs"),
        "./bower_components/purescript-*/src/**/*.purs"
    ];
    var purs_output = path.join(dist_dir, "purescript")

    execSync("mkdir -p " + purs_output);

    return require("gulp-purescript").compile({
        src: purs_sources,
        output: purs_output
    })
});

gulp.task("js", ["purs"], function() {
    var js_src = path.join(src_dir, "**/*.js");

    return gulp.src(js_src).pipe(gulp.dest(dist_dir));
});

gulp.task("browserify", ["js"], function() {
    execSync("mkdir -p " + output_dir);

    var options = {
        entries: "main.js",
        basedir: dist_dir,
    }

    return require("browserify")(options).bundle()
        .pipe(fs.createWriteStream(path.join(output_dir, output_file)));
});
    
gulp.task("prepack", ["browserify"], function(cb) {
    /* I want to do this:
     *
     * var output = prepack.prepackFileSync(path.join(output_dir, output_file));
     * return fs.createWriteStream(path.join(output_dir, output_file + ".pack"))
     *     .write(output.code);
     *
     * But the prepack API seems to have a bug. Invoking the executable works, however
     */

    execSync(
        "./node_modules/prepack/bin/prepack.js " +
        path.join(output_dir, output_file) +
        " --out " +
        path.join(output_dir, output_file + ".pack")
    );
    cb()
});
    
gulp.task("build", ["prepack"]);
